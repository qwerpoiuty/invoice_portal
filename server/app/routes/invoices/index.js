'use strict';
var router = require('express').Router();
var path = require('path')
var db = require('../../../db')
var multer = require('multer')
var multerS3 = require('multer-s3')
var env = require(path.join(__dirname, '../../../env'));
var AWS = require('aws-sdk');
var s3 = new AWS.S3({
  accessKeyId: "AKIAJ53TZGKD75PAGGRQ",
  secretAccessKey: "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"
});

s3.listBuckets(function (err, data) {
  console.log(data)
})

var upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: 'elite.invoice.portal.bucket',
    acl: 'public-read',
    contentType: multerS3.AUTO_CONTENT_TYPE,
    metadata: function (req, file, cb) {
      cb(null, {
        fieldName: file.fieldname,

      });
    },
    key: function (req, file, cb) {
      cb(null, Date.now().toString())
    }
  })
})


var ensureAuthenticated = function (req, res, next) {
  if (req.isAuthenticated()) {
    next();
  } else {
    res.status(401).end();
  }
}

function stringifyQuery(query) {
  let where = []
  Object.keys(query).forEach(key => {
    if (typeof query[key] == 'object') where.push(`"${key}" = any('{${query[key]}}')`)
    else where.push(`"${key}"='${query[key]}'`)
  })
  return where.join(' and ')
}

function stringifyBody(body) {
  let form = []
  Object.keys(body).forEach(key => {
    if (typeof body[key] == 'object') form.push(`"${key} = '{${body[key].join(',')}'`)
    else form.push(`"${key}"='${body[key]}'`)
  })
  return form.join(' , ')
}

//gets
router.get('/', (req, res, next) => {
  let query = stringifyQuery(req.query)
  db.query(`select *, (CASE WHEN "Term_ID"='CASH' OR "Term_ID"='COD' OR "Term_ID" IS NULL THEN "Transaction_Invoice_Date" ELSE "Transaction_Invoice_Date" + CAST((CAST(CAST(REPLACE("Term_ID",'DAYS','') as INT) AS CHARACTER VARYING(3)) || 'DAY') AS interval)   END) as  DueDate, (SELECT "users"."username" FROM "users" WHERE "users"."id"=CAST( CASE WHEN "Proforma"."Submit_User" IS NULL OR "Proforma"."Submit_User"='' THEN '0' ELSE "Proforma"."Submit_User" END as INT)) as SubmittedUser from "Proforma" where ${query} and "Invoice_Date" >= (CURRENT_DATE::timestamp - interval '366 days') and length(trim("Reject_User")) = 0 order by "Invoice_Number" ASC`).then(invoices => {
    res.json(invoices[0])
  })
})

router.get('/single', (req, res, next) => {
  let query = stringifyQuery(req.query)
  db.query(`select distinct * from "Proforma" where ${query} and length(trim("Reject_User")) = 0`).then(invoices => {
    res.json(invoices[0])
  })
})

router.get(`/lsr`, (req, res, next) => {
  let query = stringifyQuery(req.query)
  db.query(`select distinct "lsr_No","Inv_Status",Count(*) from "Proforma" where ${query} and "Invoice_Date" >= NOW() - interval '366 days' and "lsr_No" is not NULL group by "lsr_No","Inv_Status" order by "lsr_No"`).then(lsr => {
    res.json(lsr[0])
  })
})

router.get('/doc', (req, res, next) => {
  let query = stringifyQuery(req.query)
  db.query(`select distinct "Invoice_Number", "Voucher_Number","Job_Number","Payable_Status","Approve_User","Submit_User","Review_User","Reject_User","Reject_Remarks","Attachment_URL","syscurrency","Updated_On","Transaction_Number", "Transaction_Invoice_Date", "Environment","Currency", SUM("Amount") as "Local_Cost", SUM("CostUSD") as "USD_Cost", SUM("TotalGST") as "TotalGST", SUM("LocalAmount") as "LocalAmount" from "Payable_PL" where ${query} and length(trim("Reject_User")) = 0 group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16`).then(invoices => {
    res.json(invoices[0])
  })
})

router.get('/docTotal', (req, res, next) => {
  let query = stringifyQuery(req.query)
  db.query(`select SUM("Amount") as "SumAmount", SUM("LocalAmount") as "SumLocal","Invoice_Number","Currency" from "Payable_PL" where ${query} group by "Invoice_Number","Currency"`).then(sums => {
    res.json(sums[0])
  })
})

router.get('/payable', (req, res, next) => {
  let query = stringifyQuery(req.query)
  db.query(`select distinct "SupplierInv", "DOC_DATE","Updated_On", "Payable_Status","Attachment_URL", "Approve_User","Submit_User","Review_User","Reject_User","Reject_Remarks","FromPartyName", "Transaction_Number","Transaction_Invoice_Date","Payable_Type","ToPartyName",SUM("Amount") as "Local_Cost", SUM("GST_Amount") as "GST_Total" from "Payable_PL" where ${query} and length(trim("Reject_User")) = 0 and length("SupplierInv") <>0 group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 ORDER by "SupplierInv"`).then(docIDs => {
    res.json(docIDs[0])
  })
})

router.get('/payablereport', (req, res, next) => {
  let query = stringifyQuery(req.query)
  db.query(`SELECT "ToPartyName" as "Vendor_Name",(SELECT distinct "CUSTOMER_MASTER"."Parent_Short_Name" FROM "CUSTOMER_MASTER" WHERE "CUSTOMER_MASTER"."Environment"="Payable_PL"."Environment" AND "CUSTOMER_MASTER"."Parent_ID"="Payable_PL"."Parent_ID" limit 1) as "Cust_Parent_ID", "Voucher_Number" as "EGS_Ref_No", "SupplierInv" as "Doc_No", "DOC_DATE" as "Doc_Date", "Currency",SUM("Amount") as "Amount",SUM("LocalAmount")+ (CAST(SUM("LocalAmount"*"GST_Percent") as DECIMAL(12,2))) as "LocalAmount",SUM("LocalAmount") as "Loc_Amount_withoutGST", (SELECT "users"."username" FROM "users" WHERE "users"."id"=CAST( CASE WHEN "Payable_PL"."Approve_User" IS NULL OR "Payable_PL"."Approve_User"='' THEN '0' ELSE "Payable_PL"."Approve_User" END as INT)) as "User_ID" ,"Updated_On" as "Gate_2_Approval_Date_Time"  FROM "Payable_PL" WHERE ${query} group by 1,2,3,4,5,6,10,11  ORDER by "SupplierInv"`).then(docIDs => {
    res.json(docIDs[0])
  })
})

router.get('/payable/pnl', (req, res, next) => {
  let query = stringifyQuery(req.query)
  db.query(`select distinct * from "Payable_PL" where ${query} and length(trim("Reject_User")) = 0`).then(pls => {
    res.json(pls[0])
  })
})

router.get('/pnl', (req, res, next) => {
  let query = stringifyQuery(req.query)
  db.query(`select distinct *,(SELECT "VAT_TAX_Details"."VAT_ID" FROM "VAT_TAX_Details" WHERE "VAT_TAX_Details"."VAT_Type"="Proforma_PL"."LINE_GST_TYPE") as VAT_Type, CAST(("TotalGST"  - (("TotalGST"/(100 + ("LINE_GST_P")*100))* ("LINE_GST_P")*100)) as DECIMAL(12,2)) as "NoGst" from "Proforma_PL" where ${query}`).then(pl => {
    res.json(pl[0])
  })
})

router.get('/pnlEurope', (req, res, next) => {
  let query = stringifyQuery(req.query)
  db.query(`SELECT getvatdescription("Environment","LINE_GST_TYPE") as Description,"LINE_GST_P","LINE_GST_TYPE","VAT_Description", SUM(Qty*Amount*local_rate) AS Amount,SUM(local_rate*Qty*Amount*"LINE_GST_P") as LocalAmount,  CAST((SUM("TotalGST")  - ((SUM("TotalGST")/(100 + ("LINE_GST_P")*100))* ("LINE_GST_P")*100)) as DECIMAL(12,2)) as "NoGst",SUM("TotalGST") as "Total" FROM (SELECT "Environment", CAST (SUBSTRING (SPLIT_PART(SPLIT_PART("Details",'|',2),'x',1),'(([0-9]+.*)*[0-9]+)') AS DECIMAL) as Qty,CAST(SUBSTRING (SPLIT_PART(SPLIT_PART("Details",'|',2),'x',2),'(([0-9]+.*)*[0-9]+)') AS DECIMAL) as Amount,CAST(SUBSTRING (SPLIT_PART(SPLIT_PART("Details",'|',2),'x',3),'(([0-9]+.*)*[0-9]+)') AS DECIMAL) as local_rate,"VAT_Description","LINE_GST_P","LINE_GST_TYPE", "Currency", "Proforma_PL"."TotalGST" FROM "Proforma_PL" LEFT JOIN "VAT_TAX_Details" ON "VAT_TAX_Details"."VAT_Type"="Proforma_PL"."LINE_GST_TYPE"  where ${query} ) TBL GROUP BY "LINE_GST_P","LINE_GST_TYPE","VAT_Description","Environment" ORDER BY "LINE_GST_P"`).then(pl => {
    res.json(pl[0])
  })
})

router.get('/entityDetails/:environment', (req, res) => {
  db.query(`select * from "TaxInvoice_Address" where "Environment"='${req.params.environment}'`).then(data => {
    res.json(data[0])
  })
})

router.post('/upload', upload.single('invoice'), (req, res, next) => {
  let query = stringifyQuery(JSON.parse(req.body.query))
  db.query(`update "Payable_PL" set "Attachment_URL" = '${req.file.location}' where ${query} and length(trim("Reject_User")) = 0`).then(() => {
    res.json({
      status: 200
    })
  })
})

//router.put('/update', (req, res, next) => {
  //let query = stringifyQuery(req.body.query)
 // let body = stringifyBody(req.body.updates)
 // let callback = stringifyQuery(req.body.callback)

  // if (req.body.updates.Inv_Status === 'Submitted') {
   // if (req.body.updates.Proforma_Type === 'Invoice' || !req.body.updates.Proforma_Type) {
    //  body += `,"Transaction_Number" = getgaplesssequencenumber('${req.body.callback.Environment}', 'Invoice','${req.body.query.Consolidation_ID}',"Proforma"."Invoice_Number") ${req.body.updates.Transaction_Invoice_Date ? '' : `, "Transaction_Invoice_Date" = '${new Date().toISOString()}'`}`
   // } else {
    //  body += `,"Transaction_Number" = getgaplesssequencenumber('${req.body.callback.Environment}', '${req.body.updates.Proforma_Type}','${req.body.query.Consolidation_ID}',"Proforma"."Invoice_Number") ${req.body.updates.Transaction_Invoice_Date ? '' : `, "Transaction_Invoice_Date" = '${new Date().toISOString()}'`}`
   // }
  //}
  //db.query(`update "Proforma" set ${body} where ${query} and length(trim("Reject_User")) = 0`)
   // .then(() => {
    //  db.query(`select * from "Proforma" where ${callback} and "Invoice_Date" >= NOW() - interval '366 days' and length(trim("Reject_User")) = 0 order by "Invoice_Number" ASC`).then(invoices => {
     //   res.json(invoices[0])
     // })
   // })
// })

router.put('/update', (req, res, next) => {
  let query = stringifyQuery(req.body.query)
  let body = stringifyBody(req.body.updates)
  let callback = stringifyQuery(req.body.callback)
  let additionalqueryparam =  stringifyQuery(req.body.additionalqueryparam)
   
  if (req.body.updates.Inv_Status === 'Submitted') {
    if (req.body.updates.Proforma_Type === 'Invoice' || !req.body.updates.Proforma_Type) {
      body += ` ${req.body.updates.Transaction_Invoice_Date ? '' : `, "Transaction_Invoice_Date" = '${new Date().toISOString()}'`}`
    } else {
      body += ` ${req.body.updates.Transaction_Invoice_Date ? '' : `, "Transaction_Invoice_Date" = '${new Date().toISOString()}'`}`
    }

    if(req.body.additionalqueryparam.Consolidation == false)
    {db.query(`update "Proforma" set ${body} where ${query} and length(trim("Reject_User")) = 0`)
    .then(() => {
      db.query(`select * from TaxInvoice_SequenceUpdate('${req.body.callback.Environment}',array[${req.body.query.Invoice_Number}],0)`)
    .then(() => {
      db.query(`select * from "Proforma" where ${callback} and "Invoice_Date" >= NOW() - interval '366 days' and length(trim("Reject_User")) = 0 order by "Invoice_Number" ASC`).then(invoices => {
        res.json(invoices[0])
      })
      })
    })
  }
  else
  {
    db.query(`update "Proforma" set ${body} where ${query} and length(trim("Reject_User")) = 0`)
    .then(() => {
      db.query(`select * from TaxInvoice_SequenceUpdate('${req.body.callback.Environment}',array[${req.body.query.Consolidation_ID}], 1)`)
    .then(() => {
      db.query(`select * from "Proforma" where ${callback} and "Invoice_Date" >= NOW() - interval '366 days' and length(trim("Reject_User")) = 0 order by "Invoice_Number" ASC`).then(invoices => {
        res.json(invoices[0])
      })
      })
    })
  }
    
  }
  else
  {
    db.query(`update "Proforma" set ${body} where ${query} and length(trim("Reject_User")) = 0`)
    .then(() => {
      db.query(`select * from "Proforma" where ${callback} and "Invoice_Date" >= NOW() - interval '366 days' and length(trim("Reject_User")) = 0 order by "Invoice_Number" ASC`).then(invoices => {
        res.json(invoices[0])
      })
    })
  }
})

router.put('/updateDate', (req, res) => {
  let query = stringifyQuery(req.body.query);
  let body = stringifyBody(req.body.updates);
  db.query(`update "Proforma" set ${body} where ${query}`).then(() => {
    res.sendStatus(200)
  })
})

router.put('/payable/update', (req, res, next) => {
  let query = stringifyQuery(req.body.query)
  let body = stringifyBody(req.body.updates)

  let PayableType;
  if (req.body.updates.Payable_Type === "CN") {
    PayableType = "AP_CN";
  }
  else if (req.body.updates.Payable_Type === "DN") {
    PayableType = "AP_DN";
  }
  if (req.body.updates.Payable_Status === 'Submitted') {
    if (req.body.updates.Payable_Type === "Payable" || !req.body.updates.Payable_Type) {
      body += ` ${req.body.updates.Transaction_Invoice_Date ? '' : `, "Transaction_Invoice_Date" = '${new Date().toISOString()}'`}`
    } else {
      body += ` ${req.body.updates.Transaction_Invoice_Date ? '' : `, "Transaction_Invoice_Date" = '${new Date().toISOString()}'`}`
    }

    db.query(`update "Payable_PL" set ${body} where ${query} and length(trim("Reject_User")) = 0`)
    .then(() => {
      db.query(`select * from taxinvoice_sequenceupdatepayable('${req.body.query.Environment}',array['${req.body.query.SupplierInv.toString().replace(/,/g,"','")}'])`)
    .then(() => {
      res.json({
        status: 200
      })
      })
    })
  }
  else
  {
  db.query(`update "Payable_PL" set ${body} where ${query} and length(trim("Reject_User")) = 0`)
    .then(() => {
      res.json({
        status: 200
      })
    })
  }
})

//router.put('/payable/update', (req, res, next) => {
  //let query = stringifyQuery(req.body.query)
 // let body = stringifyBody(req.body.updates)

  //let PayableType;
 // if (req.body.updates.Payable_Type === "CN") {
 //   PayableType = "AP_CN";
 // }
 // else if (req.body.updates.Payable_Type === "DN") {
  //  PayableType = "AP_DN";
 // }
 // if (req.body.updates.Payable_Status === 'Submitted') {
 //   if (req.body.updates.Payable_Type === "Payable" || !req.body.updates.Payable_Type) {
 //     body += `,"Transaction_Number" = getgaplesssequencenumber('${req.body.query.Environment}', 'Voucher',"Payable_PL"."SupplierInv",0) ${req.body.updates.Transaction_Invoice_Date ? '' : `, "Transaction_Invoice_Date" = '${new Date().toISOString()}'`}`
 //   } else {
  //    body += `,"Transaction_Number" = getgaplesssequencenumber('${req.body.query.Environment}', '${req.body.updates.Payable_Type}','Voucher',"Payable_PL"."SupplierInv",0) ${req.body.updates.Transaction_Invoice_Date ? '' : `, "Transaction_Invoice_Date" = '${new Date().toISOString()}'`}`
  //  }
  //}
 // db.query(`update "Payable_PL" set ${body} where ${query} and length(trim("Reject_User")) = 0`)
 //   .then(() => {
//      res.json({
//        status: 200
 //     })
 //   })
//})

router.put('/bundle', (req, res, next) => {
  let query = stringifyQuery(req.body.query)
  let callback = stringifyQuery(req.body.callback)
  db.query(`update "Proforma" set "Consolidation_ID" =
    (SELECT last_value FROM consol_id)
    where ${query} and length(trim("Reject_User")) = 0`)
    .then(() => {
      db.query(`select nextval('consol_id')`)
      db.query(`select * from "Proforma" where ${callback} and length(trim("Reject_User")) = 0 order by "Invoice_Number" ASC`).then(invoices => {
        res.json(invoices[0])
      })
    })
})

router.put('/split', (req, res, next) => {
  let ids = req.body.updates
  let childID = req.body.child
  db.query(`update "Proforma" set "Consolidation_ID" = '' where "Invoice_Number" = any('{${ids}}')`).then(() => {
    db.query(`select * from "Proforma" where "Child_ID" = '${childID}' and length(trim("Reject_User")) = 0 order by "Invoice_Number" ASC`).then(invoices => {
      res.json(invoices[0])
    })
  })
})



module.exports = router
