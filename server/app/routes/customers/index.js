'use strict';
var router = require('express').Router();
var fs = require('fs')
var path = require('path')
var db = require('../../../db')
var User = db.model('user');
var Budget = db.model('budget')
var _ = require('lodash')


// var ensureAuthenticated = function(req, res, next) {
//   if (req.isAuthenticated()) {
//     next();
//   } else {
//     res.status(401).end();
//   }
// }
function stringifyQuery(query) {
  let where = []
  Object.keys(query).forEach(key => {
    if (typeof query[key] == 'object') where.push(`"${key}" = any('{${query[key]}}')`)
    else where.push(`"${key}"='${query[key]}'`)
  })
  return where.join(' and ')
}
//gets
router.get('/countries', (req, res, next) => {
  if (Object.keys(req.query).length !== 0) {
    let query = stringifyQuery(req.query)
    db.query(`select distinct "Environment" from "CUSTOMER_MASTER" where ${query} order by "Environment"`).then(countries => {
      res.json(countries[0])
    })
  } else {
    db.query(`select distinct "Environment" from "CUSTOMER_MASTER" order by "Environment"`).then(countries => {
      res.json(countries[0])
    })
  }
})

router.get('/parents', (req, res, next) => {
  let query = req.query
  let where = []
  Object.keys(query).forEach(key => {
    if (typeof query[key] == 'object') where.push(`c."${key}" = any('{${query[key]}}')`)
    else where.push(`c."${key}"='${query[key]}'`)
  })
  where = where.join(' and ')
  db.query(`select distinct c."Parent_Short_Name",c."Parent_ID",b."month",b."year" from "CUSTOMER_MASTER" c left join budgets b on c."Parent_ID" = b."Parent_ID" where ${where} order by "Parent_Short_Name"`)
    .then(customers => {
      res.json(customers[0])
    })
})

router.get('/budgets', (req, res, next) => {
  let parent_id = req.query.Parent_ID
  let environment = req.query.Environment
  db.query(
    `select COUNT(DISTINCT "Job_Number") from "Proforma" as job_count where "Parent_ID" = ${parent_id}
    and "Inv_Status" = 'Submitted'
    and "Environment" = '${environment}'
    and EXTRACT(MONTH from "Updated_On") = EXTRACT(MONTH from CURRENT_TIMESTAMP)
    and EXTRACT(YEAR from "Updated_On") = EXTRACT(YEAR from CURRENT_TIMESTAMP)
    union all
    select COUNT(DISTINCT "Job_Number") from "Proforma" as job_count where "Parent_ID" = ${parent_id}
    and "Inv_Status" = 'Submitted'
    and "Environment" = '${environment}'
    and EXTRACT(YEAR from "Updated_On") = EXTRACT(YEAR from CURRENT_TIMESTAMP)
    union all
    select SUM(pl."LocalCurrency") as month_amount from "Proforma" p
    inner join "Proforma_PL" pl
    on p."Invoice_Number" = pl."Invoice_Number"
    where p."Parent_ID" = ${parent_id}
    and "Inv_Status" = 'Submitted'
    and p."Environment" = '${environment}'
    and EXTRACT(MONTH from p."Updated_On") = EXTRACT(MONTH from CURRENT_TIMESTAMP)
    and EXTRACT(YEAR from p."Updated_On") = EXTRACT(YEAR from CURRENT_TIMESTAMP)
    union all
    select SUM(pl."LocalCurrency") as year_amount from "Proforma" p
    inner join "Proforma_PL" pl
    on p."Invoice_Number" = pl."Invoice_Number"
    where p."Parent_ID" = ${parent_id}
    and "Inv_Status" = 'Submitted'
    and p."Environment" = '${environment}'
    and EXTRACT(YEAR from p."Updated_On") = EXTRACT(YEAR from CURRENT_TIMESTAMP)`
  ).then(response => {
    let values = response[0]
    let budget = {
      mtdJobs: values[0].count,
      ytdJobs: values[1].count,
      mtdTotal: values[2].count,
      ytdTotal: values[3].count
    }
    res.json(budget)
  })
})

router.get('/allParents', (req, res, next) => {
  db.query(`select distinct c."Parent_Short_Name",c."Parent_ID", b.month,b.year
from "CUSTOMER_MASTER" c
left join "budgets" b on c."Parent_ID" = b."Parent_ID"
where c."Parent_Short_Name" is NOT NULL and c."Parent_Short_Name" <> ' ' order by "Parent_Short_Name"

`).then(parents => {
    res.json(parents[0])
  })
})

router.get('/children', (req, res, next) => {
  let query = req.query
  db.query(`select b."Parent_Short_Name",b."Parent_ID",b."Child_ID",b."Child_Short_Name","Child_Address_Line_1","Child_Address_Line_2","Child_Address_Line_3","Child_Address_Line_4","Child_Town","Child_City","Child_Country","Child_ZipCode","Child_Website", Count(*) as "Total",
count(case when "Inv_Status" ='Pending' then "Inv_Status" end ) as "Pending",
count(case when "Inv_Status" ='Approved' then "Inv_Status" end ) as "Approved",
count(case when "Inv_Status" ='New' then "Inv_Status" end ) as "New",
count(case when "Inv_Status" ='Submitted' then "Inv_Status" end ) as "Submitted"
from "CUSTOMER_MASTER" as b
LEFT OUTER JOIN "Proforma" a on b."Child_ID" = a."Child_ID" and b."Environment" = a."Environment" and a."Parent_ID" = b."Parent_ID"
where b."Environment"='${query.Environment}' and b."Parent_ID"='${query.Parent_ID}' and a."Control_Flag" = 'Y' and a."Inv_Status" <> 'Rejected'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13
order by b."Child_ID" ASC`)
    .then(clients => {
      res.json(clients[0])
    })
})
router.get('/childrenLSR', (req, res, next) => {
  let query = req.query
  db.query(`select b."Parent_Short_Name",b."Parent_ID",b."Child_ID",b."Child_Short_Name","Child_Address_Line_1","Child_Address_Line_2","Child_Address_Line_3","Child_Address_Line_4","Child_Town","Child_City","Child_Country","Child_ZipCode","Child_Website" 
from "CUSTOMER_MASTER" as b
LEFT OUTER JOIN "Proforma" a on b."Child_ID" = a."Child_ID" and b."Environment" = a."Environment" and a."Parent_ID" = b."Parent_ID"
where b."Environment"='${query.Environment}' and b."Parent_ID"='${query.Parent_ID}'  and b."Child_ID"='${query.Child_ID}'
order by b."Child_ID" ASC`)
    .then(clients => {
      res.json(clients[0])
    })
})

router.get('/vendors', (req, res, next) => {
  let query = req.query
  db.query(`select a."ToPartyID", a."ToPartyName", count(*) as "Total",
count(case when "Payable_Status" ='Pending' then "Payable_Status" end ) as "Pending",
count(case when "Payable_Status" ='Approved' then "Payable_Status" end ) as "Approved",
count(case when "Payable_Status" ='Submitted' then "Payable_Status" end ) as "Submitted",
count(case when "Payable_Status" ='New' then "Payable_Status" end ) as "New"
from
 (select distinct "SupplierInv","ToPartyID", "ToPartyName", "Parent_ID","Payable_Status"
 from "Payable_PL"
 where "Environment" = '${query.Environment}' and "Control_Flag"='Y' and length(trim("Reject_User")) = 0 and length("SupplierInv") <>0) a
 where "Parent_ID" = '${query.Parent_ID}'
group by 1,2
ORDER BY a."ToPartyName"`

  ).then(vendors => {
    res.json(vendors[0])
  })
})

router.get('/approvals', (req, res, next) => {
  console.log(__dirname)
  const json = fs.readFileSync(`${__dirname}/approvals.json`)
  res.json(JSON.parse(json.toString()))
})

router.post('/approvals', (req, res) => {
  const approvals = req.body
  fs.writeFileSync(`${__dirname}/approvals.json`, JSON.stringify(approvals))
  res.sendStatus(200)
})

router.put('/budget', (req, res, next) => {
  Budget.upsert(req.body).then(result => {
    res.json({
      header: 200,
      message: 'Budget Updated'
    })
  })
})


module.exports = router;
