'use strict';
var router = require('express').Router();
module.exports = router;

router.use('/users', require('./users'));
router.use('/invoices', require('./invoices'))
router.use('/customers', require('./customers'))
  // Make sure this is after all of
  // the registered routes!
router.use(function(req, res) {
  res.status(404).end();
});