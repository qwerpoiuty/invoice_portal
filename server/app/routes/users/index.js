'use strict';
var router = require('express').Router();
var db = require('../../../db')
var User = db.model('user');
const nodemailer = require('nodemailer');
var _ = require('lodash')


var ensureAuthenticated = function (req, res, next) {
  if (req.isAuthenticated()) {
    next();
  } else {
    res.status(401).end();
  }
}

function stringifyQuery(query) {
  let where = []
  Object.keys(query).forEach(key => {
    if (typeof query[key] == 'object') where.push(`"${key}" = any('{${query[key]}}')`)
    else where.push(`"${key}"='${query[key]}'`)
  })
  return where.join(' and ')
}

function stringifyBody(body) {
  let form = []
  Object.keys(body).forEach(key => {
    if (typeof body[key] == 'object') form.push(`"${key} = '{${body[key].join(',')}'`)
    else form.push(`"${key}"='${body[key]}'`)
  })
  return form.join(' , ')
}

//gets
router.get('/all', (req, res, next) => {
  db.query(`select * from "users" order by "username" ASC`).then(users => {
    res.json(users[0])
  })
})

router.get('/list', (req, res, next) => {
  db.query(`select username,id from "users" order by "id" ASC`).then(users => {
    res.json(users[0])
  })
})

router.get(`/proformaTasks`, (req, res, next) => {
  let where = []
  let query = req.query
  Object.keys(query).forEach(key => {
    if (typeof query[key] == 'object') where.push(`p."${key}" = any('{${query[key]}}')`)
    else where.push(`p."${key}"='${query[key]}'`)
  })
  where = where.join(' and ')
  db.query(`select p."Parent_ID", p."Environment", c."Parent_Short_Name", count(distinct "Invoice_Number")
from "Proforma" p
inner join "CUSTOMER_MASTER" c on p."Parent_ID" = c."Parent_ID" and p."Environment" = c."Environment"
where ${where}
group by 1,2,3`).then(tasks => {
    res.json(tasks[0])
  })
})

router.get(`/payableTasks`, (req, res, next) => {
  let where = []
  let query = req.query
  Object.keys(query).forEach(key => {
    if (typeof query[key] == 'object') where.push(`p."${key}" = any('{${query[key]}}')`)
    else where.push(`p."${key}"='${query[key]}'`)
  })
  where = where.join(' and ')
  db.query(`select p."Parent_ID", p."Environment", c."Parent_Short_Name", count(distinct "Invoice_Number")
from "Payable_PL" p
inner join "CUSTOMER_MASTER" c on p."Parent_ID" = c."Parent_ID" and p."Environment" = c."Environment"
where ${where}
group by 1,2,3`).then(tasks => {
    res.json(tasks[0])
  })
})

router.get(`/homepageProformaChartData`, (req, res) => {
  let query = stringifyQuery(req.query);
  let Environment = '';
  let Parent_ID = 0
  if (req.query.Environment != undefined) {
    Environment = req.query.Environment;
  }
  if (req.query.Parent_ID != undefined) {
    Parent_ID = req.query.Parent_ID;
  }
  db.query(`select * from getproformabyweek('${Environment}')`).then(rows => {
    res.json(rows[0])
  })
})

router.get(`/homepagePayableChartData`, (req, res) => {
  let query = stringifyQuery(req.query);
  let Environment = '';
  let Parent_ID = 0
  if (req.query.Environment != undefined) {
    Environment = req.query.Environment;
  }
  if (req.query.Parent_ID != undefined) {
    Parent_ID = req.query.Parent_ID;
  }
  db.query(`select * from getpayablebyweek('${Environment}')`).then(rows => {
    res.json(rows[0])
  })
})

router.get(`/proformaCount`, (req, res, next) => {
  let query = stringifyQuery(req.query)
  let Environment = '';
  let Parent_ID = 0
  if (req.query.Environment != undefined) {
    Environment = req.query.Environment;
  }
  if (req.query.Parent_ID != undefined) {
    Parent_ID = req.query.Parent_ID;
  }


  db.query(`select * from getproformacount('${Environment}','${Parent_ID}')`).then(proformaCount => {
    res.json(proformaCount[0])
  })
})

router.get(`/payableCount`, (req, res, next) => {
  let query = stringifyQuery(req.query)
  let Environment = '';
  let Parent_ID = 0
  if (req.query.Environment != undefined) {
    Environment = req.query.Environment;
  }
  if (req.query.Parent_ID != undefined) {
    Parent_ID = req.query.Parent_ID;
  }
  db.query(`select * from getpayablecount('${Environment}','${Parent_ID}')`).then(payableCount => {
    res.json(payableCount[0])
  })
})

router.get(`/homepageInvoiceStatus`, (req, res) => {
  let query = req.query
  let status = 'New'
  if (query.status) {
    status = query.status
  }
  const proformaPromise = db.query(`SELECT "Inv_Status", "Proforma"."Child_ID", "Parent_Short_Name" as FromPartyName,"Child_Short_Name" as ToPartyName, COUNT(*) as count from "Proforma" INNER JOIN "CUSTOMER_MASTER" ON "CUSTOMER_MASTER"."Parent_ID"="Proforma"."Parent_ID"  
  AND "CUSTOMER_MASTER"."Child_ID"="Proforma"."Child_ID"
  WHERE "Proforma"."Inv_Status" = '${status}' and "Proforma"."Invoice_Date" >=(CURRENT_DATE:: timestamp - interval '450 days')
  group by "Proforma"."Child_ID", "Parent_Short_Name","Child_Short_Name","Inv_Status"
  `)
  const payablePromise = db.query(`SELECT "Payable_Status", "Payable_PL"."Child_ID", "Parent_Short_Name" as FromPartyName,"Child_Short_Name" as ToPartyName, COUNT(*) as count from "Payable_PL"
  INNER JOIN "CUSTOMER_MASTER" ON "CUSTOMER_MASTER"."Parent_ID"="Payable_PL"."Parent_ID"
  AND "CUSTOMER_MASTER"."Child_ID"="Payable_PL"."Child_ID"  
   WHERE "Payable_PL"."Payable_Status" = '${status}' and "Payable_PL"."DOC_DATE" >= (CURRENT_DATE::timestamp - interval '450 days')
  group by "Payable_PL"."Child_ID", "Parent_Short_Name","Child_Short_Name","Payable_Status"
 
  `)
  Promise.all([proformaPromise, payablePromise]).then(values => {
    res.json({ proforma: values[0][0], payable: values[1][0] })
  })
})

router.get(`/homepageAging`, (req, res) => {
  let query = req.query
  let environment = ''
  if (query.Environment !== 'All') {
    environment = query.Environment
  }
  const proformaPromise = db.query(`select * from proformaaging('${environment}')`)
  const payablePromise = db.query(`select * from payableaging('${environment}')`)
  Promise.all([proformaPromise, payablePromise]).then(values => {
    res.json({ proforma: values[0][0], payable: values[1][0] })
  })
})

router.get('/stackedChart', (req, res) => {
  let query = req.query
  let environment = ''
  if (query.Environment !== 'All') {
    environment = query.Environment
  }
  const proformaPromise = db.query(`select * from proformacountforstackedchart('${environment}')`)
  const payablePromise = db.query(`select * from getpayableStackedChartcount('${environment}')`)
  Promise.all([proformaPromise, payablePromise]).then(values => {
    res.json({ proforma: values[0][0], payable: values[1][0] })
  })
})

router.get('/ytdByEntity', (req, res) => {
  let query = req.query
  let environment = ''
  if (query.Environment !== 'All') {
    environment = query.Environment
  }
  const proformaPromise = db.query(`select * from ytd_proformabyentity('${environment}')`).then(rows => {
    res.json(rows[0])
  })
})

router.get('/InvoiceLanguage/:printLang', (req, res) => {
 
  db.query(`select * from getinvoiceformatlanguage('${req.params.printLang}')`).then(rows => {
    res.json(rows[0])
  })
})

router.get('/InvoiceLanguage', (req, res) => {
  let query = req.query
  let environment = ''
  if (query.Environment !== 'All') {
    environment = query.prin
  }
  const proformaPromise = db.query(`select * from getinvoiceformatlanguage('${environment}')`).then(rows => {
    res.json(rows[0])
  })
})

router.get('/ytdJobCount', (req, res) => {
  let query = req.query
  let environment = ''
  if (query.Environment !== 'All') {
    environment = query.Environment
  }
  const jobcountPromise = db.query(`select * from proformajobcount('${environment}')`)
  const ytdpercentagePromise = db.query(`select * from getytdpercentage('${environment}')`)
  Promise.all([jobcountPromise, ytdpercentagePromise]).then(values => {
    res.json({ jobcount: values[0][0], jobpercentage: values[1][0] })
  })
})


//updates
router.put('/update', function (req, res, next) {
  const updates = req.body
  User.update(updates, {
    individualHooks: true,
    where: {
      id: req.body.id
    }
  }).then(result => {
    res.json(result)
  }).catch((err) => {
    next(err)
  })
})

router.put('/update/single', (req, res, next) => {
  User.update(req.body, {
    where: {
      id: req.body.id
    }
  }).then(result => {
    res.json({
      header: 200,
      message: 'User Updated'
    })
  }).catch(err => {
    res.json({
      header: 401,
      message: 'Problem Updating User'
    })
  })
})

router.put('/resetPassword', (req, res, next) => {
  User.update(
    req.body.updates, {
    individualHooks: true,
    where: req.body.query
  }).then(result => {
    res.json(result)
  }).catch(err => {
    next(err)
  })
})

router.put(`/forgotPassword`, (req, res, next) => {
  User.findOne({
    where: {
      username: req.body.username
    }
  }).then(user => {
    if (!user) {
      let error = new Error('Inproper Credentials')
      next(error)
    } else if (user.email !== req.body.email) {
      let error = new Error('Inproper Credentials')
      next(error)
    } else {
      User.update({
        password: 'elite123'
      }, {
        individualHooks: true,
        where: {
          username: user.username
        }
      }).then(user => {
        res.sendStatus(200)
        let transporter = nodemailer.createTransport({
          host: 'smtp-aws.eliteglobalplatform.com.',
          port: 25,
          secure: false, // true for 465, false for other ports
          auth: {}
        });
        let mailOptions = {
          from: 'noreply@elitesin.com', // sender address
          to: user.email, // list of receivers
          subject: `Your password has been reset`, // Subject line
          text: 'emailBody', // plain text body
          html: `<p>Your password has been reset to 'eliet123'</p>` // html body
        };
        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            return console.log(error);
          }
          console.log('hello', info.messageId)
        });
      })
    }
  })
})

router.put(`/defaultPassword/`, (req, res, next) => {
  User.findOne({
    where: req.query
  }).then(user => {
    user.update({
      password: 'elite123'
    }, {
      individualHooks: true
    }).then(result => {
      res.sendStatus(200)
    })
    let transporter = nodemailer.createTransport({
      host: 'smtp-aws.eliteglobalplatform.com.',
      port: 25,
      secure: false, // true for 465, false for other ports
      auth: {}
    });
    let mailOptions = {
      from: 'noreply@elitesin.com', // sender address
      to: user.email, // list of receivers
      subject: `Your password has been reset`, // Subject line
      text: 'emailBody', // plain text body
      html: `<p>Your password has been reset to the elite default password</p>` // html body
    };
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        return console.log(error);
      }
      console.log('mailed', info.messageId)
    });
  })
})

router.post('/', (req, res, next) => {
  User.findCreateFind({
    where: {
      username: req.body.username
    },
    defaults: req.body
  }).spread((user, created) => {
    if (!created) {
      var message = {
        header: 401,
        message: "That username is already registered"
      }
      res.json(message)
    } else {
      var message = {
        header: 200,
        message: "User created"
      }
      res.json(message)
    }
  })
})


//creates
router.post('/signup', function (req, res, next) {
  var user = req.body
  User.create(user).then(createdUser => {
    res.json(createdUser)
  }).catch(err => {
    next(err)
  })
})



module.exports = router;
