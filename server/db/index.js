// 'use strict';
const db = require('./_db');
const Sequelize = require('sequelize');
const User = require('./models/user')
const Budget = require('./models/budgets')
// var Bundle = require('./models/bundle')
// var Invoice = require('./models/invoice')
// var LineItem = require('./models/lineItem')


// Bundle.Invoices = Bundle.hasMany(Invoice);
// Invoice.Bundle = Invoice.belongsTo(Bundle)
// Invoice.LineItems = Invoice.hasMany(LineItem);
// Also works for `hasOne`


module.exports = db;
