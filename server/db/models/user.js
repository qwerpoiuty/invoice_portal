'use strict';
var bcrypt = require('bcryptjs');
var _ = require('lodash');
var Sequelize = require('sequelize');

var db = require('../_db');

module.exports = db.define('user', {
  username: {
    type: Sequelize.STRING
  },
  password: {
    type: Sequelize.STRING
  },
  salt: {
    type: Sequelize.STRING
  },
  role: {
    type: Sequelize.INTEGER
      /*
      0:reviewer,
      1:manager
      */
  },
  environments: {
    type: Sequelize.ARRAY(Sequelize.STRING),
    defaultValue: []
  },
  email: {
    type: Sequelize.STRING
  },
  parents: {
    type: Sequelize.ARRAY(Sequelize.STRING),
    defaultValue: []
  },
  children: {
    type: Sequelize.ARRAY(Sequelize.STRING)
  },
  modules: {
    type: Sequelize.ARRAY(Sequelize.STRING)
  },
  admin: {
    type: Sequelize.BOOLEAN
  }
}, {
  instanceMethods: {
    sanitize: function() {
      return _.omit(this.toJSON(), ['password', 'salt']);
    },
    correctPassword: function(candidatePassword) {
      return bcrypt.compareSync(candidatePassword, this.password, this.salt);
    }
  },
  hooks: {
    beforeCreate: function(user) {
      var salt = bcrypt.genSaltSync(10);
      var hash = bcrypt.hashSync(user.password, salt);
      user.salt = salt;
      user.password = hash;
    },
    beforeUpdate: function(user, optons, next) {
      if (!user.changed('password')) {
        return
      }
      var salt = bcrypt.genSaltSync(10);
      var hash = bcrypt.hashSync(user.password, salt);

      user.set('salt', salt);
      user.set('password', hash);
    }
  }
});
