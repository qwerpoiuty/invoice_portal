'use strict';
var bcrypt = require('bcryptjs');
var _ = require('lodash');
var Sequelize = require('sequelize');

var db = require('../_db');

module.exports = db.define('budget', {
  Parent_ID: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  month: Sequelize.INTEGER,
  year: Sequelize.INTEGER,
  active: Sequelize.BOOLEAN
})
