'use strict';
var _ = require('lodash');
var Sequelize = require('sequelize');

var db = require('../_db');

var permissions = db.define('permissions', {
  RoleID: Sequelize.INTEGER,
  RoleName: Sequelize.STRING,
  Application: Sequelize.STRING,
  Environment: Sequelize.ARRAY(Sequelize.STRING),
  ClientID: Sequelize.ARRAY(Sequelize.INTEGER),
  Module: Sequelize.STRING,
  Review: Sequelize.STRING,
  Approve: Sequelize.STRING,
  Leader: Sequelize.STRING,
  Admin: Sequelize.STRING
});

module.exports = permissions