var path = require('path');
var Sequelize = require('sequelize');
var env = require(path.join(__dirname, '../env'));
var db = new Sequelize(env.DATABASE_URI, {
    logging: env.LOGGING,
    pool: {
        max: 100,
        min: 0,
        idle: 200000,
        acquire: 1200000,
        idle: 1000000,
    },
    dialectOptions: {
        options: {
            statement_timeout: 60000,
            query_timeout: 60000,
            idle_in_transaction_session_timeout: 60000
        }
    },
});

module.exports = db;