var cron = require('node-cron')
var db = require('./db');
var fs = require('fs')
db.sync()

const approveInvoices = () => {
  const approvals = JSON.parse(fs.readFileSync(`${__dirname}/app/routes/customers/approvals.json`).toString())
  Object.keys(approvals).forEach(country => {
    const gate1 = []
    const gate2 = []
    Object.keys(approvals[country]).forEach(parentId => {
      const parents = approvals[country]
      if (parents[parentId].gate1) {
        gate1.push(parentId)
      }
      if (approvals[country][parentId].gate2) {
        gate2.push(parentId)
      }
    })
    db.query(`update "Proforma" set "Inv_Status" = 'Pending' where "Inv_Status" = 'New' and "Parent_ID" = any('{${gate1.join('')}}') and "Environment" = '${country}'`).then(() => {
      db.query(`update "Proforma" set "Inv_Status" = 'Approved' where "Inv_Status" = 'Pending' and "Parent_ID" = any('{${gate2.join('')}}') and "Environment" = '${country}'`).then(() => {
        console.log('finished approval')
      })
    })
  })
}

cron.schedule('*/15 * * * *', () => {
  approveInvoices()
});