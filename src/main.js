import Vue from 'vue';
import App from './App.vue';
import store from './store/index';
import router from './router';
import BootstrapVue from 'bootstrap-vue';
import moment from 'moment';
import VuejsDialog from "vuejs-dialog"

import 'vuejs-dialog/dist/vuejs-dialog.min.css';

// Tell Vue to install the plugin.
Vue.filter('formatDate', function (value) {
  if (value) {
    return moment(String(value)).format('DD MMM YYYY')
  }
});

require('./styles/app.css');
Vue.use(VuejsDialog, {
  html: true,
  loader: true,
  okText: 'Proceed',
  cancelText: 'Cancel',
  animation: 'bounce',
})
Vue.use(BootstrapVue);

new Vue({
  router,
  store,
  el: '#app',
  render: h => h(App)
})
