import axios from "axios"

export function getAllUsers() {
  return axios.get(`/api/users/all`)
}

export function getProformaTasks(query) {
  return axios.get(`/api/users/proformaTasks`, {
    params: query
  })
}

export function getPayableTasks(query) {
  return axios.get(`/api/users/payableTasks`, {
    params: query
  })
}

//
export function getProformaCount(query) {
  return axios.get(`/api/users/proformaCount`, {
    params: query
  })
}

export function getPayableCount(query) {
  return axios.get(`/api/users/payableCount`, {
    params: query
  })
}

export function homepagePayableChartData(query) {
  return axios.get(`/api/users/homepagePayableChartData`, {
    params: query
  })
}

export function homepageProformaChartData(query) {
  return axios.get(`/api/users/homepageProformaChartData`, {
    params: query
  })
}

export function hompageAging(query) {
  return axios.get(`/api/users/homepageAging`, { params: query })
}

export function getByStatus(query) {
  return axios.get(`/api/users/homepageInvoiceStatus`, {
    params: query

  })
}

export function getEntityInfo(environment) {
  return axios.get(`/api/invoices/entityDetails/${environment}`)
}
export function getStackedChart(query) {
  return axios.get(`/api/users/stackedChart`, {
    params: query
  })
}

export function GetInvoiceLanguage(printLang) {
  return axios.get(`/api/users/InvoiceLanguage/${printLang}`)
}



export function getYtdByEntity(query) {
  return axios.get(`/api/users/ytdByEntity`, {
    params: query
  })
}

export function getYtdJobCount(query) {
  return axios.get(`/api/users/ytdJobCount`, {
    params: query
  })
}

export function resetPassword(updates, query, callback = {}) {
  let body = {
    updates: updates,
    query: query,
    callback: callback
  }
  return axios.put(`/api/users/resetPassword`, body)
}

export function setDefaultPassword(query) {
  return axios.put(`/api/users/defaultPassword`, query)
}

export function forgotPassword(credentials) {
  return axios.put(`/api/users/forgotPassword`, credentials)
}

export function updateUser(body) {
  return axios.put(`/api/users/update/single`, body)
}
export function createUser(body) {
  return axios.post(`/api/users`, body)
}
