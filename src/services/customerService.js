import axios from "axios"

export function getCountries(query) {
  return axios.get(`/api/customers/countries`, {
    params: query
  })
}

export function getParents(query) {
  return axios.get('/api/customers/parents', {
    params: query
  })
}

export function getBudget(query) {
  return axios.get('/api/customers/budgets', {
    params: query
  })
}

export function getAllParents() {
  return axios.get(`/api/customers/allParents`)
}

export function getChildren(query) {
  return axios.get(`/api/customers/children`, {
    params: query
  })
}
export function getChildrenByLSR(query) {
  return axios.get(`/api/customers/childrenLSR`, {
    params: query
  })
}


export function getVendors(query) {
  return axios.get(`/api/customers/vendors`, {
    params: query
  })
}

export function getApprovals() {
  return axios.get(`/api/customers/approvals`)
}

export function postApprovals(approvals) {
  return axios.post(`/api/customers/approvals`, approvals)
}

export function updateBudget(query) {
  return axios.put(`/api/customers/budget`, query)
}
