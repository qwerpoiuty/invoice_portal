import axios from 'axios';

function upload(formData, query) {
  const url = `/api/invoices/upload`;
  formData.append('query', JSON.stringify(query))
  return axios.post(url, formData)
}

export {
  upload
}
