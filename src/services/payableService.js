import axios from "axios"

export function getPayables(query) {
  return axios.get('/api/invoices/payable', {
    params: query
  })
}

export function getPayablesReport(query) {
  return axios.get('/api/invoices/payablereport', {
    params: query
  })
}


export function getPayableInvoices(query) {
  return axios.get(`/api/invoices/doc`, {
    params: query
  })
}

export function getSingleInvoice(query) {
  return axios.get(`/api/invoices/single`, {
    params: query
  })
}

export function getDocTotal(query) {
  return axios.get(`/api/invoices/docTotal`, {
    params: query
  })
}

export function getPayablePL(query) {
  return axios.get(`/api/invoices/payable/pnl`, {
    params: query
  })
}

export function updatePayable(updates, query, callback) {
  let payload = {
    updates: updates,
    query: query,
    callback: callback
  }
  return axios.put(`/api/invoices/payable/update`, payload)
}
