import axios from "axios"

export function getProforma(query) {
  return axios.get(`/api/invoices`, {
    params: query
  })
}

export function getProformaPL(query) {
  return axios.get(`/api/invoices/pnl`, {
    params: query
  })
}

export function getProformaPLEurpoe(query) {
  return axios.get(`/api/invoices/pnlEurope`, {
    params: query
  })
}

export function getLsr(query) {
  return axios.get(`/api/invoices/lsr`, {
    params: query
  })
}

//export function updateProforma(updates, query, callback) {
  //let payload = {
   // updates: updates,
   // query: query,
   // callback: callback
 // }
 // return axios.put(`/api/invoices/update`, payload)
//}

export function updateProforma(updates, query, callback, additionalqueryparam) {
  let payload = {
    updates: updates,
    query: query,
    callback: callback,
    additionalqueryparam:additionalqueryparam
  }
  return axios.put(`/api/invoices/update`, payload)
}

export function updateDate(updates, query) {
  let payload = {
    updates, query
  }
  return axios.put('/api/invoices/updateDate', payload)
}

export function bundleProforma(updates, query, callback) {
  let payload = {
    updates: updates,
    query: query,
    callback: callback
  }
  return axios.put(`/api/invoices/bundle`, payload)
}
export function getgaplesssequencenumber(query) {
  return axios.get(`/api/invoices/getgaplesssequencenumber`, {
    params: query
  })
}

