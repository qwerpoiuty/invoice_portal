import Vue from 'vue'
import Router from 'vue-router'
import home from './components/home'
import proformaInvoices from './components/proformaInvoices'
import payableInvoices from './components/payableInvoices'
import invoiceNumbers from './components/invoiceNumbers'
import taxInvoices from './components/taxInvoices'
import reports from './components/reports'
import admin from './components/admin'
import login from './components/login'
import store from './store/index'
import axios from 'axios'
Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [

    {
      path: '/',
      name: 'Home',
      component: home,
      meta: {
        requiresLogin: true
      }
    }, {
      path: '/proforma',
      name: 'Proforma Invoices',
      component: proformaInvoices,
      meta: {
        requiresLogin: true
      }
    }, {
      path: '/payable',
      name: 'Payable Invoices',
      component: payableInvoices,
      meta: {
        requiresLogin: true
      }
    }, {
      path: '/tax',
      name: 'Tax Invoices',
      component: taxInvoices,
      meta: {
        requiresLogin: true
      }
    }, {
      path: '/login',
      name: 'Login',
      component: login
    }, {
      path: '/invoiceNumbers',
      name: 'Invoice Numbers',
      component: invoiceNumbers,
      meta: {
        requiresLogin: true
      }
    }, {
      path: '/reports',
      name: 'Reports',
      component: reports,
      meta: {
        requiresLogin: true
      }
    }, {
      path: '/admin',
      name: 'Admin',
      component: admin,
      meta: {
        admin: true
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.admin)) {
    if (store.state.admin) next()
    else {
      axios.get(`/session`).then(response => {
        let user = response.data.user
        if (user.admin) next()
        else next("/")
      }).catch(() => {
        next({
          path: "/"
        })
        console.error("Request error!");
      })
    }
  }
  if (to.matched.some(record => record.meta.requiresLogin)) {
    // You can use store variable here to access globalError or commit mutation
    if (store.state.authenticated) next()
    else {
      axios.get(`/session`).then(response => {
        if (response.status == 200) {
          next()
        }
      }).catch(() => {
        next({
          path: "/login"
        })
        console.error("Request error!");
      });
    }

  } else {
    next()
  }
})

export default router
