import {
  Pie,
} from 'vue-chartjs'

export default {
  extends: Pie,
  props: ['data', 'options'],
  watch: {
    data() {
      if (this._data._chart) {
        this._data._chart.destroy()
      }
      this.renderChart(this.data, {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
          display: false
        },
      })
    }
  },
  mounted() {
    // Overwriting base render method with actual data.

  }
}
