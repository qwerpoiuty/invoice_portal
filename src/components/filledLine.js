import {
    Line,
  } from 'vue-chartjs'
  
  export default {
    extends: Line,
    props: ['data', 'options'],
    watch: {
      data() {
        if (this._data._chart) {
          this._data._chart.destroy()
        }
        this.renderChart(this.data, {
          responsive: true,
          maintainAspectRatio: false,
          filled:true,
          plugins: {
            legend: false // Hide legend
        },
         // backgroundColor: 'rgba(255, 99, 132, 0.2)',//this.color,
          borderColor:this.color,
          scales: {
            xAxes: [{  
              display:false,
              gridLines:false,
              ticks: {
                autoSkip: false,
                maxRotation: 90,
                minRotation: 90
            }
            }],
            yAxes: [{ 
              display:false
            }]
          },
          options: {
      //  plugins: {
            legend: {
                display: false,
                labels: {
                    color: 'rgb(255, 99, 132)'
                }
           // }
        }
    }
          
        })
      }
    },
    mounted() {
      // Overwriting base render method with actual data.
  
    }
  }
  