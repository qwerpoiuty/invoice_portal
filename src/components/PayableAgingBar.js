import {
  Bar,
} from 'vue-chartjs'

export default {
  extends: Bar,
  props: ['data', 'options'],
  watch: {
    data() {
      if (this._data._chart) {
        this._data._chart.destroy()
      }
      this.renderChart(this.data, {
        responsive: true,
        maintainAspectRatio: false,
         
        interaction: {
          intersect: false,
        },
        options: {
        scales: {
          x: {
            stacked: true,
            display:false
          },
          y: {
            stacked: false,
            display:false
          }
        }
      }
      })
    }
  },
  mounted() {
    // Overwriting base render method with actual data.

  }
}
