import { Line, mixins } from 'vue-chartjs' 


export default {
  extends: Line,
  props: ['data', 'options'],
  watch: {
    data() {
      if (this._data._chart) {
        this._data._chart.destroy()
      }
      this.renderChart(this.data, {
        responsive: true,
        maintainAspectRatio: false,
        
     
    scales: {
      yAxes: [{
          display: true, 
          ticks: {
              min: 0, // minimum value
              max: 100 // maximum value
          }
      }]
  
  },
        tooltips: {
          enabled: true,
          callbacks: {
            title: function (tooltipItems) {
              return this._data.labels[tooltipItems[0].index];
            },
            label: function (tooltipItems) {
              return tooltipItems.yLabel + ' Items'
            }
          }
        }
      })
    }
  },
  
  
  mounted () {
      
  }
}
