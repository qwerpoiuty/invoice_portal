import {
  Bar,
} from 'vue-chartjs'

export default {
  extends: Bar,
  props: ['data', 'options'],
  
  watch: {
    data() {
      if (this._data._chart) {
        this._data._chart.destroy()
      }
      this.renderChart(this.data, {
        responsive: true,
        maintainAspectRatio: false,
        title: {
          display: false,
          text: 'Items For You',
          fontColor: '#858d95'
        },
        scales: {
          yAxes: [{
            ticks: {
              stepSize: 5,
              min: 1,
              beginAtZero: true,
              callback: value => {
                return Number(value.toFixed(0))
              }
            },
            scaleLabel: {
              display: true,
              labelString: '# of Items',
              fontColor: '#858d95'
            }
          }],
          xAxes: [{
            ticks: {
              autoSkip: false,
              callback: tick => {
                let limit = 10;
                if (tick.length >= limit) {
                  return tick.slice(0, tick.length).substring(0, limit - 1).trim() + '...'
                } else {
                  return tick
                }
              }
            },
            scaleLabel: {
              display: true,
              labelString: 'Parent',
              fontColor: '#858d95'
            }
          }]
        },
        tooltips: {
          enabled: true,
          callbacks: {
            title: function (tooltipItems) {
              return this._data.labels[tooltipItems[0].index];
            },
            label: function (tooltipItems) {
              return tooltipItems.yLabel + ' Items'
            }
          }
        }
      })
    }
  },
  mounted() {
    // Overwriting base render method with actual data.

  }
}
