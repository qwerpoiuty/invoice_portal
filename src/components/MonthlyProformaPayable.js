import {
  Bar,
} from 'vue-chartjs'

export default {
  extends: Bar,
  props: ['data', 'options'],
  watch: {
    data() {
      if (this._data._chart) {
        this._data._chart.destroy()
      }
      this.renderChart(this.data, {
        
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          legend: false // Hide legend
      },
      scales: {
        xAxes: [{
          stacked: true, 
          display:true,
          gridLines:true,
          ticks: {
            autoSkip: false,
            padding:5
        }
        }],
        yAxes: [{
          stacked: true,
          display:false
        }]
      },
          
          options: { plugins: {
            legend: {
              display:false
            },
            title: {
              display: true,
              text: 'Chart.js Horizontal Bar Chart'
            }
          }
        }
        
      });
    }
  },
  mounted() {
    // Overwriting base render method with actual data.

  }
}
