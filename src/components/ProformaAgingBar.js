import {
  Bar,
} from 'vue-chartjs'

export default {
  extends: Bar,
  props: ['data', 'options'],
  watch: {
    data() {
      if (this._data._chart) {
        this._data._chart.destroy()
      }
      this.renderChart(this.data, {
        responsive: true,
        maintainAspectRatio: false,
        title: {
          display: false,
          text: 'Items For You',
          fontColor: '#858d95'
        },
        interaction: {
          intersect: false,
        },
        scales: {
          xAxes: [{
            stacked: true, 
            display:true,
            gridLines:true,
            ticks: {
              padding: 4,
              autoSkip: false
          }
          }],
          yAxes: [{
            stacked: true,
            display:false
          }]
        }
      })
    }
  },
  mounted() {
    // Overwriting base render method with actual data.

  }
}
