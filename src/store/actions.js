import axios from "axios";

export const getSessionUser = ({
  commit
}) => {
  axios.get(`/session`).then(response => {
    if (response.status == 200) {
      commit('SET_USER', response.data);
    } else {
      console.error("Request error!");
    }
  }).catch(response => {
    console.log(response)
    return null
  });
};

export const getUserList = ({
  commit
}) => {
  axios.get(`api/users/list`).then(response => {
    if (response.status == 200) {
      commit('SET_USER_LIST', response.data);
    } else {
      console.log("Request error")
    }
  }).catch((response) => {
    console.log(response)
    return null
  })
}
