import Vue from 'vue'
import Vuex from 'vuex'

import * as actions from './actions'
Vue.use(Vuex)

const state = {
  user: null,
  theme: null,
  document: null,
  sortStartDate: null,
  sortEndDate: null,
  payableSearch: {},
  storedSearch: false,
  spinner: false,
  authenticated: false,
  admin: false,
  userlist: []
}

const mutations = {
  ['SET_USER'](state, data) {
    state.user = data.user
    state.theme = data.user.role
    state.authenticated = true
    state.admin = data.user.admin
  },
  ['LOGOUT'](state) {
    state.user = null
    state.theme = null
    state.authenticated = false
    state.document = null
    state.payableSearch = null
    state.storedSearch = null
    state.sortStartDate = null
    state.sortEndDate = null
  },
  ['SET_DOC_ID'](state, data) {
    state.document = data
  },
  ['SET_DATE'](state, data) {
    state.sortStartDate = data.start
    state.sortEndDate = data.end
  },
  ['START_SPINNER'](state) {
    state.spinner = true
  },
  ['STOP_SPINNER'](state) {
    state.spinner = false
  },
  ['SET_SEARCH'](state, data) {
    state.storedSearch = true
    state.payableSearch = data
  },
  ['SET_USER_LIST'](state, data) {
    let users = data.reduce((list, user) => {
      list[user.id] = user.username
      return list
    }, {})
    state.userlist = users
  }
}

const getters = {
  user: (state) => {
    return state.user
  },
  spinner: (state) => {
    return state.spinner
  }
}
export default new Vuex.Store({
  state,
  mutations,
  getters,
  actions
})
